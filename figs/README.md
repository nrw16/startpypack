# This is a Placeholder README for the repomatic repository structure

## This specific readme.md is for the following folder: figs

It can be changed by modifying the file 'readme-figs.md' in the 'inst/extdata/' folder in the package file structure, and then running the 'writemdrdas()' function with the path of the repository directory (where the DESCRIPTION file is) as the argument.

Running this function will update the 'figs_mdlv.rda' file of the 'repomatic' package, which contains the lines of text in this markdown as a vector of character elements ('figs_mdlv'), to match the contents of the 'inst/extdata/readme-figs.md' file.

This work is legally bound by the following software license: [CC-A-NS-SA-4.0][1] [^1]  
Please see the LICENSE.txt file, in the root of this repository, for further details.

[1]: https://creativecommons.org/licenses/by-nc-sa/4.0/ "CC-A-NS-SA-4.0"


[^1]: [CC-A-NS-SA-4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/)
