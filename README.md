# This the `startpypack` python package repository.

### The `startpypack` python package initializes new python packages.

Try it out!

    import startpypack

    # Make the package in the current working directory
    startpypack.makepackage("testpack")

    # Define a list of additional functions to add in
    fn = ['newfunc1','newfunc2','newfunc3']

    # Add templates for the new functions into the package
    for f in fn:
      startpypack.mkfunc("testpack",f)

This work is legally bound by the following software license: [CC-A-NS-SA-4.0][1] [^1]  
Please see the LICENSE.txt file, in the root of this repository, for further details.

[1]: https://creativecommons.org/licenses/by-nc-sa/4.0/ "CC-A-NS-SA-4.0"


[^1]: [CC-A-NS-SA-4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/)
