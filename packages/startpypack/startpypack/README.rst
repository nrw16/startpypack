The python package 'startpypack' contains functionality for creating and working with python packages.

The following websites were used as resources during implementation of this package:

Official docs:
https://packaging.python.org/installing/

Minimal structure...
Contrived package 'funniest':
https://python-packaging.readthedocs.io/en/latest/

Docstring conventions:
https://www.python.org/dev/peps/pep-0257/

Google style conventions:
http://sphinxcontrib-napoleon.readthedocs.io/en/latest/example_google.html

