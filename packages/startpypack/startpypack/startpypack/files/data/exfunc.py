def [[funcname]](arg1,arg2):
    """This function adds two numbers together.

    Args:
        arg1 (numeric): The first parameter.
        arg2 (numeric): The second parameter.

    Returns:
        res (numeric): The result of adding the two input parameters together.
    """
    res = arg1 + arg2
    return(res)
