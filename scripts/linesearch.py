# A script to experiment with searching lines of text for matches in python

# Define the search term
st = "This"

# This is a line-by-line search for a match with conditional execution
with open("../data/test.txt") as search:
    for line in search:
        # remove '\n' at end of line with rstrip
        # and subset to first 4 elements with slicing
        tl = line.rstrip()[0:4]
        print(tl)
        if st == tl:
            print(line)
