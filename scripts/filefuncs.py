# This script is to test file copying/moving functionality in python


################################################################################
# File moving
################################################################################

import os

# The os.rename function essentially 'moves' a file by changing it's name
#  Change it once
#os.rename('../data/test.txt','../data/testrenamed.txt')
#  Change it back
#os.rename('../data/testrenamed.txt','../data/test.txt')

################################################################################
# File copying
################################################################################

import shutil

# This copyfile function from the shutil package works to copy a file
#shutil.copyfile('../data/test.txt', '../data/testcopy.txt')
shutil.copyfile('../data/testcopy.txt', '../data/test.txt')

