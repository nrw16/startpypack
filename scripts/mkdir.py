# A python script to test out functionality for making new directories

import os

directory = 'testdir'

if not os.path.exists(directory):
    os.makedirs(directory)
