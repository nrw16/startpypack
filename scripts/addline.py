# A script to test adding in an additional line to a file

# Itertools is needed to flatten the list of lists from string slicing
import itertools

# This is somewhat convoluted but...
#  It works to insert a new line at a particular place in the file

# Open the file and get the lines of text
f = open("../data/test.txt",'r+')
rl = f.readlines()
print(rl)
print(type(rl))
# 'Add in' a new line to create a list of strings
nl = "this is a new line!\n"
nls = [[rl[0]],[nl],rl[1:]]
nlsf = list(itertools.chain(*nls))
print(nls)
print(nlsf)
f.close()

# Write the list of strings back into the file
f = open("../data/test.txt",'w+')
f.writelines(nlsf)
f.close()

