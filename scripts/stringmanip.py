# This is a script to test out string manipulation functionality in python

ts = 'test string'
tsa = 'addition'

tsm = ts + ' ' + tsa

print(ts)
print(tsa)
print(tsm)
print(tsm[5:11])

# And also matching within a string using '.find()' function
print(tsm[tsm.find('add'):])

