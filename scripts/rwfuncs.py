# This script is to test out pythons reading/writing functionality


# Open to read out the lines into a 'str' object
f = open('../data/test.txt', 'r+')

# This read function is preferrable to appending to a list object
fl = f.read()
print(type(fl))

print(fl)

# This replace function can be used to find-and-replace targets
# even throughout multiline text strings.
#  Simple replace
#flr = fl.replace("file","fish")
#  What happens when the replacement text is shorter than original?
#flr = fl.replace("file","fly")
#  What happens when the replacement text is longer than original?
flr = fl.replace("file","flibberdigibbit")
print(flr)

# Set the writing location to the beginning of the file
#f.seek(0)
#f.write(flr)

# Turns out theres no need to read into a list object like this...
#  Even though it does work
#ml = list()
#ml.append(f.readline())
#ml.append(f.readline())
#ml.append(f.readline())
#ml.append(f.readline())
#for l in ml:
#  print l
#mr = ml[0].replace("file.","file that has been edited.")
#print(mr)

# Set the writing location to the beginning of the file
#f.seek(0)
#f.write(mr)

f.close()

# Copy the text into a new file
f = open('../data/testcopy.txt', 'w+')
f.write(fl)
f.close()

# Totally rewrite the lines of the original file
f = open('../data/test.txt', 'w')
f.write('replace!')
f.close



